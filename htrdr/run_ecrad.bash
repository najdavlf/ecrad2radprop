#!/bin/bash
ecrad=../bin/ecrad
dinp=ecrad_input/
dout=ecrad_output/

rm -f $dout/output*.nc
rm -f $dout/radiative_properties*.nc

# run nominal
$ecrad $dinp/config.nam $dinp/clearsky.nc $dout/output_nominal.nc > ecrad_run.log 2>&1
cp radiative_properties.nc $dout/radiative_properties_nominal.nc

# run nowater
$ecrad $dinp/config.nam $dinp/clearsky_nowater.nc $dout/output_nowater.nc >> ecrad_run.log 2>&1
cp radiative_properties.nc $dout/radiative_properties_nowater.nc

# run water vapor concentrations for look up table
iw=1
keep_going=true
while [ "$keep_going" = "true" ]
do
  if [ $iw -lt 10 ]; then
	  str=00$iw
  elif [ $iw -lt 100 ]; then
	  str=0$iw
  elif [ $iw -lt 1000 ]; then
	  str=$iw
  else
	  echo "Too many points"
	  exit 1
  fi
  file_in=$dinp/water$str.nc
  if [ -e $file_in ]; then
	  file_out=$dout/output_water$str.nc
	  file_rad=$dout/radiative_properties_water$str.nc
	  $ecrad $dinp/config.nam $file_in $file_out >> ecrad_run.log 2>&1
	  cp radiative_properties.nc $file_rad
	  iw=$((iw+1))
  else
	  keep_going=false
  fi
done # while keep_going=true

exit 0
