      subroutine add_slash_if_absent(st)
      implicit none
      include 'max.inc'
c
c     Purpose: to add a slash character '/' at the end of a character string
c     when there is none.
c
c     I/O:
c       + st: character string
c

c     I/O
      character*(Nchar_mx) st
c     temp
      character*1 sstring
      integer n
      integer strlen
      character*(Nchar_mx) label
      label='subroutine add_slash_if_absent'

      sstring='/'

      n=strlen(st)
      if (st(n:n).ne.sstring(1:1)) then
         st=st(1:strlen(st))//sstring(1:1)
      endif

      return
      end
