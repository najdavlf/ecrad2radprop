The main program of interest is "make_optprop.exe"

This program will generate the "ecrad_opt_prop.txt" data file
that is needed for cloudy atmospheric scenes rendering.
See the associated documentation for additional information
(Doc/gas_opt_prop.pdf).
-------------------------------------------------------------------

0- Prerequisites: the following librairies must be installed (linux):
libnetcdf
libnetcdff

The current directory should be placed within the main ECRAD
directory, at the same level that the "bin", "data", "driver", etc
directories.
-------------------------------------------------------------------

1- Compilation: use the Makefile script.

It itself runs the "f0" script multiple times to produce its module
files. It usually takes running the "f0" script 3 times
in order to produce the "make_optprop.exe" executable file.

You may have to edit the Makefile compilation script in order to
edit the paths to include and library files. The sections that
should be modified are:

export IDIR=/usr/include
export LDIR=/usr/lib64
export F77=gfortran
-------------------------------------------------------------------

2- How it works:
The program needs the "data.in" input data file; the user should
specifiy in this file the number of points to use for the water
vapor mixing ratio tabulation, as well as the required minimum
and maximum values over the relative humidity (variable 'q').

The program will also need to access a file that describes the required
ECRAD atmospheric profile to use: by default, it uses the
"ecrad_input/input.nc" file. This can be changed by editing the
"make_optprop.F90" source file, and modifying the "path_to_original" and
"original_file" variables within the user input section.  Then the
executable should be recompiled.

An alternative is to link the desired input file to ecrad_input/input.nc
as a symbolic file.
-------------------------------------------------------------------

3- Usage: just run the executable:
./make_optprop.exe

This should produce, among other files, the "ecrad_opt_prop.txt" file
-------------------------------------------------------------------
