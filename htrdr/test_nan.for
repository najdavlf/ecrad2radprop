c     HR_ppart (http://www.meso-star.com/en_Products.html) - This file is part of HR_ppart
c     Copyright (C) 2015-2016 - M�so-Star - Vincent Eymet
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      subroutine test_nan_dble(value,is_nan)
      implicit none
      include 'max.inc'
c
c     Purpose: to test whether the provided double-precision value is a NaN or not
c
c     Input:
c       + value: double-precision value to test
c     
c     Output:
c       + is_nan: true if 'value' is a NaN; false otherwise
c
c     I/O
      double precision value
      logical is_nan
c     temp
      integer t1,t2
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine test_nan_dble'
c
      
      if (value.le.0.0D+0) then
         t1=0
      else
         t1=1
      endif
      if (value.ge.0.0D+0) then
         t2=0
      else
         t2=1
      endif
      
      if ((t1.eq.1).and.(t2.eq.1)) then
         is_nan=.true.
      else
         is_nan=.false.
      endif

      return
      end
