set -e
path=/home/nvillefranque/work/ecrad2radprop

if [ $# -ne 1 ] 
then
  echo Usage: $0 input_1D.nc
  exit 1
fi

inpabs=`realpath $1`
inp1D=`echo $inpabs | awk -F"/" '{ print $NF }'`
dirinp=${inpabs%$inp1D}

outrad=out_clear_$inp1D
outgaz=SW_rad_ppt_$inp1D

dirrad=$HOME/work/dephy/dephy2ecrad/outputs/
dirgaz=$HOME/work/data/GAZ/
direcrad=$HOME/work/rayonnement/ecrad_master/

CONFF=config_clear.nam

cd $path

ln -sf $dirinp/$inp1D ./$inp1D

# First exec ecRad to compute radiative properties
sed -i 's;directory_name.*=.*,;directory_name = "'$direcrad'/data",;g' $CONFF
$direcrad/bin/ecrad $CONFF $inp1D $dirrad/$outrad > logs/log_ecrad_$$ 2>&1
mv radiative_properties.nc radiative_properties_$inp1D

# Then exec read_optical_properties to get the data
sed -e "s/_IN_/"$inp1D"/g" namesbase.txt > names.txt
./read_optical_properties.exe > logs/log_read_opt_prop_$$

# Then write the data into netCDF
./gaz_write2ncdf > logs/log_gaz_write2ncdf_$$
mv SW_rad_ppt.nc ${dirgaz}/$outgaz

#rm $inp1D radiative_properties.nc
mv names.txt atm.txt ck_lw.txt ck_sw.txt od_abs_sw_tot.txt ssa_sw.txt incoming_sw_flux.txt asymmetry_sw.txt ecrad_opt_prop.txt txt/
rm radiative_properties_$inp1D $inp1D
