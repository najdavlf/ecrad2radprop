Programme "read_optical_properties":

+ Pr�requis:

1) Avoir fait tourner Ecrad (pour l'instant config ciel clair):
> ../bin/ecrad ./config_clear.nam ./input.nc ./output.nc

Ecrad va g�n�rer "output.nc", mais �galement "radiative_properties.nc" du fait qu'on l'a explicitement
demand� dans "config.nam" (do_save_radiative_properties = true)
et le fichier d'entr�e "input.nc"
(� l'origine, c'�tait le fichier i3rc_mls_cumuls.nc bas� sur l'original fourni
avec Ecrad, mais toutes les variables relatives aux nuages ont �t� mises �
z�ro:
overlap_param
cloud_fraction
q_ice
q_liquid
re_ice
re_liquid
inv_cloud_effective_size
fractional_std
)

Le nom des fichiers input.nc et radiative_properties.nc que l'on va traiter
doit �tre inscrit dans le fichier names.txt (premi�re ligne = input, deuxi�me
ligne = radiative_properties). Le fichier names.txt va �tre lu par le programme
read_optical_properties qui va lire les variables physiques dans input.nc et
les variables optiques dans radiative_properties.nc (voir 2).

2) Le fichier "input.nc" doit �tre pr�sent dans le r�pertoire courant, d'une
part pour pouvoir faire tourner Ecrad, et d'autre part il sera ouvert par le
programme "read_optical_properties" de fa�on � pouvoir acc�der aux variables
suivantes:
pressure_hl
temperature_hl
skin_temperature
height_hl
q
lw_emissivity
sw_albedo

Le fichier "radiative_properties.nc" doit �galement �tre pr�sent; il sera
ouvert par "read_optical_properties" pour acc�der aux variables suivantes:
planck_hl
incoming_sw
od_lw
od_sw
ssa_sw
asymmetry_sw

3) Compilation de "read_optical_properties.exe" (script "f1"):
- les fichiers suivants ont �t� pris de Ecrad:
abor1.F90
yomlun_ifsaux.F90
parkind1.F90
radiation_io.F90
easy_netcdf.F90

- les r�pertoires suivants ont �t� ajo�t�s lors de la compilation; � adapter sur une autre machine bien entendu:
/opt/local_include pour fichiers netcdf[*].h
/opt/local/lib pour les librairies netcdf

- il peut �tre n�cessaire de lancer le script "f1" plusieurs fois d'affil�e pour effectivement produire
l'ex�cutable "read_optical_properties.exe", du fait de l'utilisation de modules dans le code Ecrad.

Ne pas h�siter � ouvrir le code source (read_optical_properties.F90) et � l'adapter aux besoins bien entendu.




+ SORTIES:

1) atm.txt: profil atmosph�rique; par ligne: l'altitude (m), la pression (Pa), la temp�rature (K).

2) ck_lw.txt:
ligne 1: nombre de mailles atmosph�riques
ligne 2: nombre d'intervalles spectraux dans le LW
puis, par intervalle spectral:
- nombre de points de quadrature pour l'intervalle
- longueur d'onde min (microns), longueur d'onde max (microns), �missivit� du sol
- valeurs des poids de quadrature pour l'intervalle
- pour chaque point de quadrature: le coefficient d'absorption (m^-1), pour toutes les mailles atmosph�riques

3) ck_sw.txt: idem, mais pour le SW; la ligne des coefficients d'absorption pour chaque point de quadrature est
suivie d'une ligne o� figurent les coefficients de diffusion (m^-1) de toutes les mailles atmosph�riques, pour
le point de quadrature courant.

4) od_abs_sw_tot.txt:
Pour chaque intervalle spectral SW:
- longueur d'onde min (microns), longueur d'onde max (microns), �missivit� du sol
- nombre de points de quadrature de l'intervalle
- pour chaque point de quadrature:
+ le poids de quadrature
+ l'�paisseur optique d'absorption cumul�e sur la colonne

Ce fichier a �t� utilis� dans un test de transfert radiatif pour v�rifier la coh�rence sur les �paisseurs optiques
cumul�es sur toute la hauteur de l'atmosph�re.

5) ssa_sw.txt:
Pour chaque intervalle spectral SW:
- longueur d'onde min (microns), longueur d'onde max (microns)
- nombre de points de quadrature pour l'intervalle courant
- pour chaque points de quadrature: l'alb�do de diffusion simple pour toutes les mailles atmosph�riques

6) asymmetry_sw.txt:
Pour chaque intervalle spectral SW:
- longueur d'onde min (microns), longueur d'onde max (microns)
- nombre de points de quadrature pour l'intervalle courant
- pour chaque points de quadrature: le param�tre d'asym�trie pour toutes les mailles atmosph�riques

7) incoming_sw_flux.txt:
- ligne 1: nombre d'intervalles spectraux dans le SW
- lignes suivantes: longueur d'onde min de l'intervalle (microns), longueur d'onde max de l'intervalle (microns)
et flux solaire incident au sommet de l'atmosph�re, int�gr� sur l'intervalle (W/m^2 horizontal).

ATTENTION: ce fichier a �t� g�n�r� pour v�rifier la coh�rence des flux solaires incidents au sommet; il
s'agit de la valeur lue dans "radiative_properties.nc", mais il ne faut pas utiliser ces valeurs dans un calcul
de transfert SW, elles ne sont pas correctes (ou alors je n'ai pas compris ce qu'elles repr�sentent).

8) ecrad_opt_prop.txt:
Fichier ASCII regroupant toutes les grandeurs n�cessaires pour effectuer des calculs de transfert radiatif
en configuration ciel clair; le fichier est comment� de fa�on � �tre lisible par un humain. La seule grandeur
physique qui ne figure pas dans ce fichier est le flux solaire incident au sommet de l'atmosph�re, int�gr�
par intervalle SW... mais j'ai un programme d�di� pour celle-ci, me demander au besoin.

Dans tous les fichiers, les intervalles spectraux sont rang�s par ordre croissant de longueur d'onde.
