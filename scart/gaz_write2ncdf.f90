      program write2ncdf

        use netcdf

        implicit none 
        double precision, parameter ::pi=3.141592654
        character(len = *),parameter::SW_FILE_NAME="SW_rad_ppt.nc"
        character(len = *),parameter::LW_FILE_NAME="LW_rad_ppt.nc"
        character(len = *),parameter::atm_FILE="atm.txt"
        character(len = *),parameter::SW_k_FILE="ck_sw.txt"
        character(len = *),parameter::LW_k_FILE="ck_lw.txt"
        character(len = *),parameter::SW_ssa_FILE="ssa_sw.txt"
        character(len = *),parameter::SW_assym_FILE="assymetry_sw.txt"
        !"
        character(len = *),parameter::ISF_FILE="true_incoming_sw_flux.txt"
        !"
        integer ios,i,j,k
        integer, parameter :: NDIMS=3
        integer :: SWNBD, LWNBD, NBZ, NBK
        integer :: SW_nu_id, LW_nu_id, SW_band_id, LW_band_id
        integer :: quadrature_id, levels_id, layers_id, nodim_id
        ! dimids = NBK, S(L)WNBD, NBZ  ; wg_dimids = NBK, S(L)WNBD
        integer :: SW_dimids(NDIMS), LW_dimids(NDIMS)
        integer :: SW_wg_dimids(NDIMS-1), LW_wg_dimids(NDIMS-1)
        integer :: SW_ncid, totISFvarid, NCISFvarid, ISFvarid
        integer :: SW_nuvarid, LW_nuvarid, hlvarid, zvarid, tvarid
        integer :: pvarid, nbkvarid ! 1D, dim NBD+1 & NBZ+1 & NBD
        integer :: SW_wgvarid, LW_wgvarid ! quadrature weights
        integer :: SW_kavarid, SW_ksvarid, LW_kavarid ! abs and scatt coefficients
        integer :: SW_ssavarid, SW_assymvarid ! single scatt albedo & assym factor 
        double precision :: tot_ISF
        double precision,allocatable,dimension(:) :: norm_cum_ISF
        double precision,allocatable,dimension(:) :: ISF, hltab, ztab
        double precision,allocatable,dimension(:) :: SW_nutab, LW_nutab
        double precision,allocatable,dimension(:) :: ttab, ptab, nbktab
        double precision,allocatable,dimension(:,:)   :: SW_wg, LW_wg
        double precision,allocatable,dimension(:,:,:) :: SW_ka, SW_ks, LW_ka
        double precision,allocatable,dimension(:,:,:) :: SW_ssa, SW_assym
        double precision :: tmp,y,yj,yn,x,xj,xn,dy,dz


!      ###### Read atm #####

        open(11, file=atm_FILE, status="old", &
                iostat=ios)
        if (ios.ne.0) then
                write(*,*) "atm file could not be opened"
        else
          read(11,*) NBZ
          allocate(hltab(NBZ-1),ztab(NBZ),ttab(NBZ),ptab(NBZ))
          do i=1,NBZ
            read(11,*) ztab(i), ptab(i), ttab(i)
            if (i.gt.1) then
                    dz=ztab(i)-ztab(i-1)
                    hltab(i-1) = ztab(i-1)+dz/2.
            endif
          enddo
        endif
        close(11)

!       #### Read SW k ####

        open(11, file=SW_k_FILE, status="old", &
                iostat=ios) 
        if (ios.ne.0) then
                write(*,*) "SW k file could not be opened"
        else 
          NBK=0
          read(11,*) NBZ
          read(11,*) SWNBD
          allocate(SW_nutab(SWNBD+1))
          allocate(norm_cum_ISF(SWNBD),ISF(SWNBD), nbktab(SWNBD))
          do i=1,SWNBD
            read(11,*) nbktab(i)
            read(11,*) SW_nutab(i), SW_nutab(i+1), x
            do j=1,int(nbktab(i))*2+1
              read(11,*)
            enddo
          enddo
          NBK=maxval(nbktab)
        endif
        close(11)
        allocate(SW_wg(NBK,SWNBD), SW_ka(NBZ,NBK,SWNBD), SW_ks(NBZ,NBK,SWNBD))
        allocate(SW_ssa(NBZ,NBK,SWNBD))
!       allocate(SW_assym(NBK,SWNBD,NBZ))
        SW_wg = 0.
        open(11, file=SW_k_FILE, status="old")
        open(12, file=SW_ssa_FILE, status="old")
!       open(13, file=SW_assym_FILE, status="old")
        open(14, file=ISF_FILE, status="old")
        read(11,*)  ! nbz
        read(11,*)  ! nb bd
        read(14,*)  ! nb bd
        do i=1,SWNBD
            read(11,*) ! nbk
            read(11,*) ! numin, numax, emiss
            read(12,*) ! numin, numax
            read(12,*) ! nbk 
!           read(13,*) ! numin, numax
!           read(13,*) ! nbk 
            read(11,*) SW_wg(1:int(nbktab(i)),i)
            read(14,*) x,y, ISF(i)
            do j=1,int(nbktab(i))
              read(11,*) SW_ka(1:NBZ,j,i)
              read(11,*) SW_ks(1:NBZ,j,i)
              read(12,*) SW_ssa(1:NBZ,j,i)
!             read(13,*) SW_assym(1:NBZ,j,i)
            enddo
        enddo
        close(11)
        close(12)
!       close(13)
        close(14)

        ! compute cumulative normalized ISF
        norm_cum_ISF(1) = ISF(1)
        do i=2,SWNBD
          norm_cum_ISF(i)=norm_cum_ISF(i-1)+ISF(i)
        enddo
        tot_ISF = norm_cum_ISF(SWNBD)
        norm_cum_ISF = norm_cum_ISF/norm_cum_ISF(SWNBD)

!       ###### NetCDF ######

!       Write results in netcdf file
        ! Create file
        call check( nf90_create(SW_FILE_NAME, NF90_CLOBBER, SW_ncid) )

        ! Define dimensions
        call check( nf90_def_dim(SW_ncid, "vertical_levels", NBZ+1, levels_id) )
        call check( nf90_def_dim(SW_ncid, "vertical_layers", NBZ,   layers_id) )
        call check( nf90_def_dim(SW_ncid, "g_points",     NBK,      quadrature_id) )
        call check( nf90_def_dim(SW_ncid, "narrow_bands", SWNBD,    SW_band_id) )
        call check( nf90_def_dim(SW_ncid, "wavelengths",  SWNBD+1,  SW_nu_id) )
        call check( nf90_def_dim(SW_ncid, "nodim", 1,               nodim_id) )
        SW_dimids  =    (/ layers_id, quadrature_id, SW_band_id /) ! SW ka, ks, ssa
        SW_wg_dimids  = (/ quadrature_id, SW_band_id /)  ! SW wg

        ! Define variables
        ! nc_def_var(ncid, dimid, name, longname, units, varid)
        !integer :: SW_ncid, SW_nuvarid, LW_nuvarid, hlvarid, zvarid, tvarid, pvarid, nbkvarid ! 1D, dim NBD+1 & NBZ+1 & NBD
        !integer :: SW_wgvarid, LW_wgvarid ! quadrature weights
        !integer :: SW_kavarid, SW_ksvarid, LW_kavarid ! abs and scatt coefficients
        !integer :: SW_ssavarid, SW_assymvarid ! single scatt albedo & assym factor 

        call nc_def_var(SW_ncid, SW_dimids,"ka", &
                   "Absorption coefficient at quadrature point", "km-1", &
                   SW_kavarid)
        call nc_def_var(SW_ncid, SW_dimids,"ks", &
                   "Scattering coefficient at quadrature point", "km-1", &
                   SW_ksvarid)
        call nc_def_var(SW_ncid, SW_dimids,"ssa", &
                   "Single scattering albedo at quadrature point", "-", &
                   SW_ssavarid)
        call nc_def_var(SW_ncid, SW_wg_dimids,"wg_points", &
                   "Quadrature weights in the band", "-", &
                   SW_wgvarid)
        call nc_def_var(SW_ncid, (/SW_nu_id/), "wavelengths", &
                   "Wavelengths at bands interfaces", "nanometers", &
                   SW_nuvarid) 
        call nc_def_var(SW_ncid, (/levels_id/), "vertical_levels", &
                   "Height of vertical levels", "km", &
                   zvarid) 
        call nc_def_var(SW_ncid, (/layers_id/), "vertical_layers", &
                   "Height of the center of the vertical layers", "km", &
                   hlvarid) 
        call nc_def_var(SW_ncid, (/levels_id/), "temperature", &
                   "Temperature at vertical levels", "K", &
                   tvarid) 
        call nc_def_var(SW_ncid, (/levels_id/), "pressure", &
                   "Pressure at vertical levels", "Pa", &
                   pvarid) 
        call nc_def_var(SW_ncid, (/SW_band_id/),"g_points_nb", &
                   "Number of quadrature points in the band", "-", &
                   nbkvarid)
        call nc_def_var(SW_ncid, (/SW_band_id/),"ISF", &
                   "Incoming Solar Flux", "Wm-2", &
                   ISFvarid)
        call nc_def_var(SW_ncid, (/SW_band_id/),"norm_cum_ISF", &
                   "Normalized cumulative Incoming Solar Flux", "-", &
                   NCISFvarid)
        call nc_def_var(SW_ncid, (/nodim_id/),"tot_ISF", &
                   "Normalized cumulative Incoming Solar Flux", "-", &
                   totISFvarid)

        ! end definition
        call check( nf90_enddef(SW_ncid) )

        ! set fields
        call check( nf90_put_var(SW_ncid, SW_nuvarid, SW_nutab*1.E3) )
        call check( nf90_put_var(SW_ncid, zvarid, ztab*1.E-3) )
        call check( nf90_put_var(SW_ncid, hlvarid, hltab*1.E-3) )
        call check( nf90_put_var(SW_ncid, tvarid, ttab) )
        call check( nf90_put_var(SW_ncid, pvarid, ptab) )
        call check( nf90_put_var(SW_ncid, nbkvarid, nbktab) )
        call check( nf90_put_var(SW_ncid, ISFvarid, ISF) )
        call check( nf90_put_var(SW_ncid, NCISFvarid, norm_cum_ISF) )
        call check( nf90_put_var(SW_ncid, totISFvarid, tot_ISF) )
        call check( nf90_put_var(SW_ncid, SW_wgvarid, SW_wg) )
        call check( nf90_put_var(SW_ncid, SW_kavarid, SW_ka*1.E3) )
        call check( nf90_put_var(SW_ncid, SW_ksvarid, SW_ks*1.E3) )
        call check( nf90_put_var(SW_ncid, SW_ssavarid,SW_ssa) )

        ! close file
        call check( nf90_close(SW_ncid) )

        deallocate(norm_cum_ISF, ISF, SW_nutab, hltab, ztab, ttab, ptab, nbktab)
        deallocate(SW_wg, SW_ka, SW_ks, SW_ssa)
        
        contains
         subroutine check(status)
          integer, intent ( in) :: status
    
          if(status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped"
          end if
         end subroutine check

         subroutine nc_def_var(ncid,dimid,name,longname,units,varid)
           integer, intent(in) :: ncid
           integer, dimension(:), intent(in) :: dimid
           character(len=*), intent(in) :: name, longname, units 
           integer, intent(out) :: varid


           call check( nf90_def_var(ncid, name, NF90_DOUBLE, dimid, varid) )
           call check(nf90_put_att(ncid,varid,"long_name",longname) )
           call check(nf90_put_att(ncid,varid,"units",units) )

         end subroutine

      end program
